import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'exam-one'
  },


  {
    path: 'exam-one',
    loadChildren: () => import('projects/exam-one/src/lib/exam-one.module').then(m => m.ExamOneModule),
  },

  {
    path: 'exam-two',
    loadChildren: () => import('projects/exam-two/src/lib/exam-two.module').then(m => m.ExamTwoModule),
  },


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
