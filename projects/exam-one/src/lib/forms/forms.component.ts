import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'lib-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  contactUsForm = new FormGroup({
    fullname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    contact: new FormControl('', [Validators.required]),
    meetingSchedule: new FormControl('', [Validators.required]),
    meetingDate: new FormControl({ value: '', disabled: true }, [Validators.required]),
    meetingTime: new FormControl({ value: '', disabled: true }, [Validators.required]),
    message: new FormControl(''),
  });

  matcher = new MyErrorStateMatcher();


  constructor() { }

  ngOnInit(): void {
  }

  getDays(): string[] {
    const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', ' Saturday', 'Sunday'];
    return days;
  }

  submit() {
    console.log(this.contactUsForm);
  }


  log(x: any) {
    console.log(x);
  }

  clearValue(control: FormControl) {
    control.setValue('');

  }

  setDate(datePicker: any) {
    console.log(datePicker.value);
    datePicker.value ? this.meetingTime.enable() : this.meetingTime.disable();
  }

  setSelected(matSelect: MatSelectChange) {
    matSelect.value === 'no' ? this.meetingDate.disable() : this.meetingDate.enable();
  }

  get fullname(): FormControl {
    return this.contactUsForm.get('fullname') as FormControl;
  }

  get email(): FormControl {
    return this.contactUsForm.get('email') as FormControl;
  }

  get contact(): FormControl {
    return this.contactUsForm.get('contact') as FormControl;
  }

  get meetingSchedule(): FormControl {
    return this.contactUsForm.get('meetingSchedule') as FormControl;
  }

  get meetingDate(): FormControl {
    return this.contactUsForm.get('meetingDate') as FormControl;
  }

  get meetingTime(): FormControl {
    return this.contactUsForm.get('meetingTime') as FormControl;
  }

  get message(): FormControl {
    return this.contactUsForm.get('message') as FormControl;
  }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
