import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactUsComponent } from './contact-us.component';
import { SiteFormsModule } from '../forms/forms.module';
import { AngularMaterialModule } from '../../angular-material.module';



@NgModule({
  declarations: [
    ContactUsComponent
  ],
  imports: [
    CommonModule,
    SiteFormsModule,
    AngularMaterialModule
  ],
  exports: [
    ContactUsComponent
  ]
})
export class ContactUsModule { }
