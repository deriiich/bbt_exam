import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { AngularMaterialModule } from '../../angular-material.module';



@NgModule({
  declarations: [
    FooterComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [
    FooterComponent
  ]
})
export class FooterModule { }
