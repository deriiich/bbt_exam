import { NgModule } from '@angular/core';
import { BannerModule } from './banner/banner.module';
import { ContactUsModule } from './contact-us/contact-us.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ExamOneRoutingModule } from './exam-one-routing.module';
import { ExamOneComponent } from './exam-one.component';
import { FooterModule } from './footer/footer.module';
import { SiteFormsModule } from './forms/forms.module';
import { NavigationModule } from './navigation/navigation.module';



@NgModule({
  declarations: [
    ExamOneComponent
  ],
  imports: [
    SiteFormsModule,
    ContactUsModule,
    BannerModule,
    NavigationModule,
    ExamOneRoutingModule,
    DashboardModule,
    FooterModule
  ],
  exports: [
    ExamOneComponent
  ]
})
export class ExamOneModule { }
