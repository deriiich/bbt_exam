import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner.component';
import { AngularMaterialModule } from '../../angular-material.module';



@NgModule({
  declarations: [
    BannerComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [
    BannerComponent
  ]
})
export class BannerModule { }
