import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'lib-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  pages: any[] = [];
  selectedIndex = 0;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.pages = this.getListOfPages();
  }

  getListOfPages() {
    return [
      { title: 'Book keeping & payroll' },
      { title: 'Financial & GST' },
      { title: 'KiwiSaver advice' },
      { title: 'Insuring people' },
      { title: 'Tax & business advice' },
      { title: 'Trustee services' },
      { title: 'Mortgage advice' },
      { title: 'Go to Test 2' }
    ];
  }

  onTabChanged(event: any) {
    this.selectedIndex = event.index;
    if(this.selectedIndex === 7){
      this.router.navigateByUrl('exam-two');
    }
  }

}
