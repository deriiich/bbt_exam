import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamOneComponent } from './exam-one.component';


const routes: Routes = [
  {
    path: '',
    component: ExamOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamOneRoutingModule { }
