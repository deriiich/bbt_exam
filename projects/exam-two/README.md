# ExamTwo

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Code scaffolding

Run `ng generate component component-name --project exam-two` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project exam-two`.
> Note: Don't forget to add `--project exam-two` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build exam-two` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build exam-two`, go to the dist folder `cd dist/exam-two` and run `npm publish`.

## Running unit tests

Run `ng test exam-two` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
