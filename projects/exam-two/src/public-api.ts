/*
 * Public API Surface of exam-two
 */

export * from './lib/exam-two.service';
export * from './lib/exam-two.component';
export * from './lib/exam-two.module';
