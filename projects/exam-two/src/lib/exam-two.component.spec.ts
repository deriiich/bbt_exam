import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamTwoComponent } from './exam-two.component';

describe('ExamTwoComponent', () => {
  let component: ExamTwoComponent;
  let fixture: ComponentFixture<ExamTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
