import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowMeetingComponent } from './show-meeting.component';


const routes: Routes = [{
  path: '',
  component: ShowMeetingComponent
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowMeetingRoutingModule { }
