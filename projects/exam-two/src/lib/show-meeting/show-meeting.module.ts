import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowMeetingRoutingModule } from './show-meeting-routing.module';
import { AngularMaterialModule } from 'projects/exam-one/src/angular-material.module';
import { ShowMeetingComponent } from './show-meeting.component';
import { MeetingComponent } from './meeting/meeting.component';


@NgModule({
  declarations: [ShowMeetingComponent, MeetingComponent],
  imports: [
    CommonModule,
    ShowMeetingRoutingModule,
    AngularMaterialModule
  ],
  exports: [
    ShowMeetingComponent
  ]
})
export class ShowMeetingModule { }
