import { Component, Input, OnInit } from '@angular/core';
import { Meeting } from '../../models/meeting';

@Component({
  selector: 'meeting-component',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  @Input() set meeting(meeting: Meeting[]) {
    console.log(meeting)
    this.setMeetingObject(meeting);
  }

  meetingObj: Meeting[] = {} as Meeting[];

  constructor() { }

  ngOnInit(): void {
  }

  setMeetingObject(meeting: Meeting[]) {
    this.meetingObj = meeting;
  }
}
