import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, Subject, Subscription, takeUntil } from 'rxjs';
import { MeetingService } from '../meeting-service.service';

@Component({
  selector: 'lib-show-meeting',
  templateUrl: './show-meeting.component.html',
  styleUrls: ['./show-meeting.component.css']
})
export class ShowMeetingComponent implements OnInit, OnDestroy {

  meetings: any;
  meetings$: Subscription = new Subscription;
  onDestroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private meetingService: MeetingService) { }

  ngOnInit(): void {
    this.meetings$ = this.meetingService.getMeeting().pipe(takeUntil(this.onDestroy$), map(x => {
      this.meetings = x;
    })).subscribe();
  }

  ngOnDestroy() {
    this.onDestroy$.next(true);
    this.onDestroy$.unsubscribe();
  }

  clearSchedule() {
    this.meetingService.clearSchedule();
    this.onDestroy$.next(true);

    this.meetings$ = this.meetingService.getMeeting().pipe(takeUntil(this.onDestroy$), map(x => {
      this.meetings = x;
    })).subscribe();
  }
}
