export interface Meeting {
    fullname: string;
    email: string;
    contact: number;
    meetingSchedule: string;
    meetingDate: string;
    meetingTime: string;
    message: string;
}
