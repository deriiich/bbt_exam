import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Meeting } from './models/meeting';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  meetings: Meeting[] = [
    {
      fullname: 'My Full name',
      email: 'secret@gmail.com',
      contact: 12345,
      meetingSchedule: "no",
      meetingDate: '',
      meetingTime: '',
      message: 'This is a test message'
    }
  ];

  constructor() {

  }

  createMeeting(meeting: Meeting) {
    console.log(meeting);
    this.meetings.push(meeting);
    console.log(this.meetings);
  }

  getMeeting(): Observable<Meeting[]> {
    return of(this.meetings);
  }

  clearSchedule() {
    this.meetings = [];

  }

}
