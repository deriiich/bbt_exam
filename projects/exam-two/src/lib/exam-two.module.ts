import { NgModule } from '@angular/core';
import { ExamTwoRoutingModule } from './exam-two-routing.module';
import { ExamTwoComponent } from './exam-two.component';
import { NavigationModule } from './navigation/navigation.module';


@NgModule({
  declarations: [
    ExamTwoComponent,
  ],
  imports: [
    ExamTwoRoutingModule,
    NavigationModule
  ],
  exports: [
    ExamTwoComponent
  ]
})
export class ExamTwoModule { }
