import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'lib-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  pages: any[] = [];
  selectedIndex = 0;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.pages = this.getListOfPages();
  }

  getListOfPages() {
    return [
      { title: 'Create Meeting' },
      { title: 'Show Meetings' },
      { title: 'Go to Test 1' }
    ];
  }

  onTabChanged(event: any) {
    this.selectedIndex = event.index;

    switch (this.selectedIndex) {
      case 0: {
        this.router.navigate(['exam-two/create-meeting']);
        break;
      }
      case 1: {
        this.router.navigate(['exam-two/show-meetings']);
        break;
      }
      case 2: {
        this.router.navigate(['/exam-one']);
        break;
      }
    }

  }

}
