import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamTwoComponent } from './exam-two.component';

const routes: Routes = [
    {
        path: '',
        component: ExamTwoComponent,

        children: [
            {
                path: '',
                redirectTo: 'create-meeting',
                pathMatch: 'full'
            },
            {
                path: 'create-meeting',
                loadChildren: () => import('./create-meeting/create-meeting.module').then(m => m.CreateMeetingModule)
            },
            {
                path: 'show-meetings',
                loadChildren: () => import('./show-meeting/show-meeting.module').then(m => m.ShowMeetingModule)
            },

        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExamTwoRoutingModule { }
